﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CallDataEvaluataion.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

    <script src="Scripts/jQuery.js"></script>
    <script src="Scripts/underscore-min.js"></script>
    <script src="Scripts/jstz.min.js"></script>
    <script src="Scripts/moment.js"></script>
    <script src="Scripts/moment-timezone-with-data.js"></script>
    <script src="Scripts/moment-timezone.js"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="Scripts/Default.js"></script>
</head>
<body>
    <div id="mainFormCntnr" style="display: none;">
        <form role="form" id="myForm" class="modal-body row">
            <div class="form-group col-md-6">
                <label for="txtCRAreaCode">CRAreaCode:</label>
                <input type="text" class="form-control" id="txtCRAreaCode">
            </div>
            <div class="form-group col-md-6">
                <label for="txtListDescription">ListDescription:</label>
                <input type="text" class="form-control" id="txtListDescription">
            </div>
            <div class="form-group col-md-6">
                <label for="txtLeadID">LeadID:</label>
                <input type="text" class="form-control" id="txtLeadID">
            </div>
            <div class="form-group col-md-6">
                <label for="txtLname">Lname:</label>
                <input type="text" class="form-control" id="txtLname">
            </div>
            <div class="form-group col-md-6">
                <label for="txtFname">Fname:</label>
                <input type="text" class="form-control" id="txtFname">
            </div>
            <div class="form-group col-md-6">
                <label for="txtAddress1">Address1:</label>
                <input type="text" class="form-control" id="txtAddress1">
            </div>
            <div class="form-group col-md-6">
                <label for="txtAddress2">Address2:</label>
                <input type="text" class="form-control" id="txtAddress2">
            </div>
            <div class="form-group col-md-6">
                <label for="txtCity">City:</label>
                <input type="text" class="form-control" id="txtCity">
            </div>
            <div class="form-group col-md-6">
                <label for="txtState">State:</label>
                <input type="text" class="form-control" id="txtState">
            </div>
            <div class="form-group col-md-6">
                <label for="txtZip">Zip:</label>
                <input type="text" class="form-control" id="txtZip">
            </div>
            <div class="form-group col-md-6">
                <label for="txtOriginCountryCode">OriginCountryCode:</label>
                <input type="text" class="form-control" id="txtOriginCountryCode">
            </div>
            <div class="form-group col-md-6">
                <label for="txtProgramName">ProgramName:</label>
                <input type="text" class="form-control" id="txtProgramName">
            </div>
            <div class="form-group col-md-6">
                <label for="txtSourceType">SourceType:</label>
                <input type="text" class="form-control" id="txtSourceType">
            </div>
            <div class="form-group col-md-6">
                <label for="txtEmail">Email:</label>
                <input type="email" class="form-control" id="txtEmail">
            </div>
            <div class="form-group col-md-6">
                <label for="txtHomePhone">HomePhone:</label>
                <input type="number" class="form-control" id="txtHomePhone">
            </div>
            <div class="form-group col-md-6">
                <label for="txtWorkPhone">WorkPhone:</label>
                <input type="text" class="form-control" id="txtWorkPhone">
            </div>
            <div class="form-group col-md-6">
                <label for="txtWorkExtension">WorkExtension:</label>
                <input type="text" class="form-control" id="txtWorkExtension">
            </div>
            <div class="form-group col-md-6">
                <label for="txtCellPhone">CellPhone:</label>
                <input type="number" class="form-control" id="txtCellPhone">
            </div>
            <div class="form-group col-md-6">
                <label for="txtBrands">Brands:</label>
                <input type="text" class="form-control" id="txtBrands">
            </div>
            <div class="form-group col-md-6">
                <label for="txtToCity">ToCity:</label>
                <input type="text" class="form-control" id="txtToCity">
            </div>
            <div class="form-group col-md-6">
                <label for="txtToState">ToState:</label>
                <input type="text" class="form-control" id="txtToState">
            </div>
            <div class="form-group col-md-6">
                <label for="txtToZip">ToZip:</label>
                <input type="text" class="form-control" id="txtToZip">
            </div>
            <div class="form-group col-md-6">
                <label for="txtDestinationCountryCode">DestinationCountryCode:</label>
                <input type="text" class="form-control" id="txtDestinationCountryCode">
            </div>
            <div class="form-group col-md-12">
                <a href="javascript:void(0);" class="btn btn-default" id="cmdSubmitData">Submit</a>
            </div>
        </form>
    </div>
    <div id="dayValidationMsgCntnr" style="display: none;">
        <h1>Form submission is valid from Monday to Friday</h1>
        <h1>Between 8 AM to 5PM</h1>
    </div>
</body>
</html>
