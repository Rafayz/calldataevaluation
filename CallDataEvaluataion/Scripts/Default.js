﻿/// <reference path="jQuery.js" />
/// <reference path="underscore-min.js" />
/// <reference path="jstz.min.js" />
/// <reference path="moment.js" />
/// <reference path="moment-timezone.js" />
/// <reference path="moment-timezone-with-data.js" />


var CallDataEvaluator = {
    SendCallData: function (callRecord) {
        try {

            var data = { callRecord: callRecord };

            $.ajax({
                type: "POST",
                url: "Default.aspx/SubmitCallRecord",
                contentType: "application/json",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (result) {
                    alert('Data submitted succesfully');
                },
                error: function (x, h, r) {
                }
            });
        }
        catch (ex) {
        }
    }
};

$(document).ready(function () {
    try {
        $(document).on("click", "#cmdSubmitData", function (e) {
            try {
                e.preventDefault();
                e.stopPropagation();

                var errors = validateForm();
                if (!errors.length) {

                    var data = {};

                    data.CRDate = getCurrentDate();
                    data.CRTimeZone = getCurrentTimeZone_Abbr();
                    data.CRAreaCode = $("#txtCRAreaCode").val();
                    data.ListDescription = $("#txtListDescription").val();
                    data.LeadID = $("#txtLeadID").val();
                    data.Lname = $("#txtLname").val();
                    data.Fname = $("#txtFname").val();
                    data.Address1 = $("#txtAddress1").val();
                    data.Address2 = $("#txtAddress2").val();
                    data.City = $("#txtCity").val();
                    data.State = $("#txtState").val();
                    data.Zip = $("#txtZip").val();
                    data.OriginCountryCode = $("#txtOriginCountryCode").val();
                    data.ProgramName = $("#txtProgramName").val();
                    data.SourceType = $("#txtSourceType").val();
                    data.Email = $("#txtEmail").val();
                    data.HomePhone = $("#txtHomePhone").val();
                    data.WorkPhone = $("#txtWorkPhone").val();
                    data.WorkExtension = $("#txtWorkExtension").val();
                    data.CellPhone = $("#txtCellPhone").val();
                    data.Brands = $("#txtBrands").val();
                    data.ToCity = $("#txtToCity").val();
                    data.ToState = $("#txtToState").val();
                    data.ToZip = $("#txtToZip").val();
                    data.DestinationCountryCode = $("#txtDestinationCountryCode").val();


                    CallDataEvaluator.SendCallData(data);
                }
                else {
                    alert("Errors: \n" + errors);
                }

                
                return false;
            }
            catch (Ex) {
            }
        });

        if (validateTimeAndDayOfSubmission()) {
            $("#mainFormCntnr").show();
            $("#dayValidationMsgCntnr").hide();
        }
        else {
            $("#mainFormCntnr").hide();
            $("#dayValidationMsgCntnr").show();
        }
    }
    catch (ex) {
    }
});


function validateTimeAndDayOfSubmission() {
    try {
        var dayName = moment().format('dddd');

        if (dayName === 'Saturday' || dayName === 'Sunday') {
            return false;
        }

        var currentHr = moment().hour();

        if (!(currentHr >= 18 && currentHr <= 23)) {
            return false;
        }

        return true;
    }
    catch (Ex) {
    }
}

function validateForm() {
    try {
        var errors = "";
        if (!validatePhoneNumber($("#txtHomePhone").val())) {
            errors += (errors.length !== 0 ? "\n" : "") + "Home Phone number should be of 10 digits.";
        }
        if (!validatePhoneNumber($("#txtWorkPhone").val())) {
            errors += (errors.length !== 0 ? "\n" : "") + "Work Phone number should be of 10 digits.";
        } 
        if (!validatePhoneNumber($("#txtCellPhone").val())) {
            errors += (errors.length !== 0 ? "\n" : "") + "Cell Phone number should be of 10 digits.";
        }

        return errors;
    }
    catch (Ex) {
    }
}

function getCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;

    return today;
}

function getCurrentTimeZone_Abbr() {
    try {
        var ctzName = jstz.determine();

        var currentTimeZone = moment().tz(ctzName.name()).format('z');

        return currentTimeZone;
    }
    catch (Ex) {
        return "EST";
    }
}

function validatePhoneNumber(number) {
    try {
        var re = /^\d{10}$/; ///^(\+91-|\+91|0)?\d{10}$/;
        if (number.toString().match(re)) {
            return true;
        }
        else {
            return false;
        }
    }
    catch (ex) {
        return true;
    }

}