﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using CallDataEvaluataion.Models;
using System.Xml.Linq;

namespace CallDataEvaluataion
{
    [System.Web.Script.Services.ScriptService()]
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod()]
        public static object SubmitCallRecord(CallRecordRow callRecord)
        {
            object resultObj = null;

            try
            {
                XDocument xDoc = callRecord.GetXMLData();
                string FileName = callRecord.Fname + "_" + getUnixTimeStamp();

                xDoc.Save(HttpContext.Current.Server.MapPath("/UploadedFiles") + "/" + FileName + ".xml");

                return resultObj;
            }
            catch (Exception ex)
            {
                return resultObj;
            }

        }

        private static string getUnixTimeStamp()
        {
            try
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                return unixTimestamp.ToString();
            }
            catch(Exception ex)
            {
                return "";
            }
            
        }
    }
}