﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallDataEvaluataion.Models
{
    public class CallRecordRow
    {
        public string CRDate { get; set; }
        public string CRTimeZone { get; set; }
        public string CRAreaCode { get; set; }
        public string ListDescription { get; set; }
        public string LeadID { get; set; }
        public string Lname { get; set; }
        public string Fname { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string OriginCountryCode { get; set; }
        public string ProgramName { get; set; }
        public string SourceType { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string WorkExtension { get; set; }
        public string CellPhone { get; set; }
        public string Brands { get; set; }
        public string ToCity { get; set; }
        public string ToState { get; set; }
        public string ToZip { get; set; }
        public string DestinationCountryCode { get; set; }


        public XDocument GetXMLData()
        {
            XDocument xInput = null;
            xInput = XDocument.Parse("<CallRecordRow></CallRecordRow>");
            xInput.Root.Add(new XElement("CRDate", CRDate));
            xInput.Root.Add(new XElement("CRTimeZone", CRTimeZone));
            xInput.Root.Add(new XElement("CRAreaCode", CRAreaCode));
            xInput.Root.Add(new XElement("ListDescription", ListDescription));
            xInput.Root.Add(new XElement("LeadID", LeadID));
            xInput.Root.Add(new XElement("Lname", Lname));
            xInput.Root.Add(new XElement("Fname", Fname));
            xInput.Root.Add(new XElement("Address1", Address1));
            xInput.Root.Add(new XElement("Address2", Address2));
            xInput.Root.Add(new XElement("City", City));
            xInput.Root.Add(new XElement("State", State));
            xInput.Root.Add(new XElement("Zip", Zip));
            xInput.Root.Add(new XElement("OriginCountryCode", OriginCountryCode));
            xInput.Root.Add(new XElement("ProgramName", ProgramName));
            xInput.Root.Add(new XElement("SourceType", SourceType));
            xInput.Root.Add(new XElement("Email", Email));
            xInput.Root.Add(new XElement("HomePhone", HomePhone));
            xInput.Root.Add(new XElement("WorkPhone", WorkPhone));
            xInput.Root.Add(new XElement("WorkExtension", WorkExtension));
            xInput.Root.Add(new XElement("CellPhone", CellPhone));
            xInput.Root.Add(new XElement("Brands", Brands));
            xInput.Root.Add(new XElement("ToCity", ToCity));
            xInput.Root.Add(new XElement("ToZip", ToZip));
            xInput.Root.Add(new XElement("DestinationCountryCode", DestinationCountryCode));
            return xInput;
        }
    }
}
